/**
 * Created by techno23 on 2/12/13.
 */
function dbReplication() {

    var CouchServerUser = 'amit';
    var CouchServerPassword = 'mourya';
    var CouchServerIP='192.168.0.190'
    var CouchServerDB = 'task_manager';
    var CouchServerPort = '37792';
    var task = [];
    var db = new PouchDB(CouchServerDB);
    var count = 0;
    var onChange;

    onChange = function(doc){

        if(!doc._conflicts){
            return;
        }

        collectConflicts(doc._conflicts, function(docs) {
            var master, prop, _i, _j, _len, _len1;
            master = docs.sort(byTime).unshift();
            for (_i = 0, _len = docs.length; _i < _len; _i++) {
                doc = docs[_i];
                for (_j = 0, _len1 = doc.length; _j < _len1; _j++) {
                    prop = doc[_j];
                    if (!(__indexOf.call(master, prop) >= 0)) {
                        master[prop] = doc[prop];
                    }
                }
            }
            db.put(master, function(err) {
                var _k, _len2, _results;
                if (!err) {
                    _results = [];
                    for (_k = 0, _len2 = docs.length; _k < _len2; _k++) {
                        doc = docs[_k];
                        _results.push(db.remove(doc));
                    }
                    return _results;
                }
            });
        });
    };

    filter = function(doc) {

        if(doc.type=="profile" && doc._id==localStorage.mob) {
            return true;
        }
        if(doc.type=="task") {
            var mem = doc.members;
            if(doc.admin==localStorage.mob){
                task[task.length]=doc._id;
                return true;
            }
            for(var i=0;i<mem.length;i++){
                if(mem==localStorage.mob){
                    task[task.length]=doc._id;
                    return true;
                }
            }
            return false;
        }
        if(doc.type=="comment"){
            for(var i=0;i<task.length;i++){
                if(doc.task==task[i]){
                    return true;
                }
            }
        }
        return false;

    }

    db.replicate.to('http://'+CouchServerUser+':'+CouchServerPassword+'@'+CouchServerIP+':'+CouchServerPort+'/'+CouchServerDB, {
        onChange : onChange
    });
    db.replicate.from('http://'+CouchServerUser+':'+CouchServerPassword+'@'+CouchServerIP+':'+CouchServerPort+'/'+CouchServerDB, {
        onChange : onChange
    });

};
